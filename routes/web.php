<?php

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\NFLFantasy\Controllers')
    ->prefix('dashboard/nfl')
    ->name('dashboard.nfl.')
    ->group(function () {
        Route::resource('teams', 'TeamsController')->except(['show', 'create']);
        Route::resource('games', 'GamesController')->except(['show', 'create']);
        Route::resource('picks', 'PicksController')->only(['index', 'store']);
        Route::resource('results', 'ResultsController')->only(['index', 'show']);
        Route::resource('winners', 'WinnersController')->only(['edit', 'update']);
    });
