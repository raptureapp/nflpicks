@extends('rapture::layouts.dashboard')

@section('content')
    @heading
        Enter Winners

        @slot('after')
            <a href="{{ route('dashboard.nfl.games.index') }}" class="btn-pill">
                <em class="far fa-reply" aria-hidden="true"></em> @lang('rapture::actions.return')
            </a>
        @endslot
    @endheading

    @statuses

    <div class="content-builder">
        <div class="container">
            <form method="post" action="{{ route('dashboard.nfl.winners.update', $week) }}">
                @csrf
                @method('PUT')

                <div class="row">
                    <div class="content">
                        <div class="box">
                            <ul class="matches">
                                @foreach ($week->games as $game)
                                <li>
                                    <button type="button" data-id="{{ $game->team1->id }}" class="{{ $game->team1->id === $game->winner_id ? 'active' : '' }}">
                                        <div class="team1">
                                            {{ $game->team1->name }}
                                        </div>
                                        <div class="team-logo">
                                            <img src="{{ asset($game->team1->logo) }}">
                                        </div>
                                    </button>
                                    <div class="spacer">VS</div>
                                    <button type="button" data-id="{{ $game->team2->id }}" class="{{ $game->team2->id === $game->winner_id ? 'active' : '' }}">
                                        <div class="team-logo">
                                            <img src="{{ asset($game->team2->logo) }}">
                                        </div>
                                        <div class="team2">
                                            {{ $game->team2->name }}
                                        </div>
                                    </button>
                                    <input type="hidden" name="game[{{ $game->id }}]" value="{{ $game->winner_id }}">
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="sidebar">
                        <button type="submit" class="btn full">@lang('rapture::actions.save')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('nfl/js/winners.js') }}"></script>
@endpush

@push('styles')
    <link rel="stylesheet" href="{{ asset('nfl/css/style.css') }}">
@endpush
