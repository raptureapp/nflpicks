@extends('rapture::layouts.dashboard')

@section('content')
    @heading
        @lang('nfl::games.index.title')

        @slot('after')
            @can('nfl.games.create')
                <new-entry action="{{ route('dashboard.nfl.games.store') }}" placeholder="@lang('nfl::games.new_placeholder')" name="label">@lang('nfl::games.new_item')</new-entry>
            @endcan
        @endslot
    @endheading

    @statuses

    <div class="container">
        <table class="table striped">
            <tr>
                <th>@lang('rapture::field.name')</th>
                <th class="action"></th>
            </tr>
            @foreach ($weeks as $games)
                <tr>
                    <td>{{ $games->label }}</td>
                    <td class="action">
                        @can('nfl.games.edit')
                        @if ($games->locked())
                        <a href="{{ route('dashboard.nfl.winners.edit', $games->id) }}" title="Set winners">
                            <em class="far fa-trophy"></em>
                        </a>
                        @endif
                        <a href="{{ route('dashboard.nfl.games.edit', $games->id) }}" aria-label="@lang('rapture::actions.edit')">
                            <em class="far fa-pencil" aria-hidden="true" title="@lang('rapture::actions.edit_model', ['model' => $games->label])"></em>
                        </a>
                        @endcan
                        @can('nfl.games.destroy')
                        <delete-button action="{{ route('dashboard.nfl.games.destroy', $games->id) }}" aria-label="@lang('rapture::actions.delete')" title="@lang('rapture::actions.delete_model', ['model' => $games->label])"></delete-button>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </table>

        {{ $weeks->links() }}
    </div>
@endsection
