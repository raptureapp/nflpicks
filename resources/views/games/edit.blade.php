@extends('rapture::layouts.dashboard')

@section('content')
    @heading
        @lang('nfl::games.edit.title')

        @slot('after')
            <a href="{{ route('dashboard.nfl.games.index') }}" class="btn-pill">
                <em class="far fa-reply" aria-hidden="true"></em> @lang('rapture::actions.return')
            </a>
        @endslot
    @endheading

    @statuses

    <div class="content-builder">
        <div class="container">
            <form method="post" action="{{ route('dashboard.nfl.games.update', $week) }}">
                @csrf
                @method('PUT')

                <div class="row">
                    <div class="content">
                        <div class="box">
                            <div class="form-field">
                                <label for="name">@lang('rapture::field.name') <span class="required">@lang('rapture::field.required')</span></label>
                                <input type="text" name="label" id="name" value="{{ old('label', $week->label) }}">
                            </div>
                        </div>

                        <match-selection :teams='@json($teams)' :games='@json($week->games)'></match-selection>
                    </div>
                    <div class="sidebar">
                        <button type="submit" class="btn full">@lang('rapture::actions.save')</button>

                        <div class="widget">
                            <div class="form-field">
                                <label><em class="far fa-calendar-exclamation"></em>Pick cut off</label>
                                <input type="text" name="cut_off" class="datepicker" value="{{ old('cut_off', $week->cut_off) }}">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('components')
    <script src="{{ asset('nfl/js/app.js') }}"></script>
@endpush

@push('styles')
    <link rel="stylesheet" href="{{ asset('nfl/css/style.css') }}">
@endpush
