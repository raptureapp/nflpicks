@extends('rapture::layouts.dashboard')

@section('content')
    @heading
        @lang('teams::package.edit.title')

        @slot('after')
            <a href="{{ route('dashboard.teams.index') }}" class="btn-pill">
                <em class="far fa-reply" aria-hidden="true"></em> @lang('rapture::actions.return')
            </a>
        @endslot
    @endheading

    @statuses

    <div class="content-builder">
        <div class="container">
            <form method="post" action="{{ route('dashboard.teams.update', $team) }}">
                @csrf
                @method('PUT')

                <div class="row">
                    <div class="content">
                        <div class="box">
                            <div class="form-field">
                                <label for="name">@lang('rapture::field.name') <span class="required">@lang('rapture::field.required')</span></label>
                                <input type="text" name="name" id="name" value="{{ old('name', $team->name) }}">
                            </div>
                        </div>
                    </div>
                    <div class="sidebar">
                        <button type="submit" class="btn full">@lang('rapture::actions.save')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
