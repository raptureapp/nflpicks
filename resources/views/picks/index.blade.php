@extends('rapture::layouts.dashboard')

@section('content')
    @heading
        @lang('nfl::games.index.title')
    @endheading

    <div class="tab-wrapper">
        <ul class="tab-headings">
            @foreach ($weeks as $week)
            <li class="tab{{ $currentWeek && $currentWeek->id === $week->id ? ' active' : '' }}"><a href="#tab{{ $week->id }}">{{ $week->label }}</a></li>
            @endforeach
        </ul>
    </div>

    @statuses

    @foreach ($weeks as $week)
        <div class="tab-body{{ $currentWeek && $currentWeek->id === $week->id ? ' active' : '' }}" id="tab{{ $week->id }}">
            <p class="cut-off">Close{{ $week->allowed() ? 's' : 'd' }} {{ $week->closes() }} @ 7 PM</p>
            <ul class="matches">
                @foreach ($week->games as $game)
                <li data-match="{{ $game->id }}">
                    <button type="button" class="team1{{ $game->pick && $game->pick->team_id === $game->team1->id ? ' active' : '' }}{{ $game->team1->id === $game->winner_id ? ' winner' : '' }}{{ !is_null($game->winner_id) ? ' locked' : '' }}" data-id="{{ $game->team1->id }}"{{ !$week->allowed() ? ' disabled' : '' }}>
                        <span>{{ $game->team1->name }}</span>
                        <img src="{{ $game->team1->logo }}">
                    </button>
                    <div class="spacer">VS</div>
                    <button type="button" class="team2{{ $game->pick && $game->pick->team_id === $game->team2->id ? ' active' : '' }}{{ $game->team2->id === $game->winner_id ? ' winner' : '' }}{{ !is_null($game->winner_id) ? ' locked' : '' }}" data-id="{{ $game->team2->id }}"{{ !$week->allowed() ? ' disabled' : '' }}>
                        <img src="{{ $game->team2->logo }}">
                        <span>{{ $game->team2->name }}</span>
                    </button>
                </li>
                @endforeach
            </ul>
        </div>
    @endforeach
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('nfl/css/picks.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('nfl/js/picks.js') }}"></script>
@endpush
