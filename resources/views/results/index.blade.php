@extends('rapture::layouts.dashboard')

@section('content')
    @heading
        Results
    @endheading

    <div class="tab-wrapper">
        <ul class="tab-headings">
            @foreach ($weeks as $week)
            <li class="tab{{ $currentWeek->id === $week->id ? ' active' : '' }}"><a href="#tab{{ $week->id }}">{{ $week->label }}</a></li>
            @endforeach
        </ul>
    </div>

    @statuses

    @foreach ($weeks as $week)
        <div class="tab-body{{ $currentWeek->id === $week->id ? ' active' : '' }}" id="tab{{ $week->id }}">
            @if ($winners->has($week->id) && $winners->get($week->id)->count() > 0)
                <div class="standings">
                    @foreach ($winners->get($week->id)->take(3) as $index => $result)
                    <div class="standing">
                        <span class="name">{{ $result->pluck('user.name')->implode(', ') }}</span>
                        <span class="total">
                            <em class="fal fa-trophy"></em> {{ $result->first()['correct'] }}
                        </span>
                        <div class="step">
                            <div class="inner"></div>
                        </div>
                    </div>
                    @endforeach
                    @for ($i = 3 - $winners->get($week->id)->count(); $i > 0; $i--)
                    <div class="standing">
                        <div class="step">
                            <div class="inner"></div>
                        </div>
                    </div>
                    @endfor
                </div>
                @if ($winners->get($week->id)->count() > 3)
                    <ol class="results">
                        @foreach ($winners->get($week->id)->slice(3) as $result)
                            <li>
                                <span class="name">{{ $result->pluck('user.name')->implode(', ') }}</span>
                                <span class="correct"><em class="fal fa-trophy"></em> {{ $result->first()['correct'] }}</span>
                            </li>
                        @endforeach
                    </ol>
                @endif
            @else
                <div class="standings">
                    @for ($i = 3; $i > 0; $i--)
                    <div class="standing">
                        <div class="step">
                            <div class="inner"></div>
                        </div>
                    </div>
                    @endfor
                </div>
            @endif
        </div>
    @endforeach
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('nfl/css/picks.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('nfl/js/picks.js') }}"></script>
@endpush
