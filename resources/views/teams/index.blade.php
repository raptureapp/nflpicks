@extends('rapture::layouts.dashboard')

@section('content')
    @heading
        @lang('nfl::teams.index.title')

        @slot('after')
            @can('nfl.teams.create')
                <new-entry action="{{ route('dashboard.nfl.teams.store') }}" placeholder="@lang('nfl::teams.new_placeholder')" name="name">@lang('nfl::teams.new_item')</new-entry>
            @endcan
        @endslot
    @endheading

    @statuses

    <div class="container">
        <table class="table striped">
            <tr>
                <th></th>
                <th>@lang('rapture::field.name')</th>
                <th class="action"></th>
            </tr>
            @foreach ($teams as $team)
                <tr>
                    <td class="team-logo">
                        <img src="{{ asset($team->logo) }}" alt="{{ $team->name }}">
                    </td>
                    <td>{{ $team->name }}</td>
                    <td class="action">
                        @can('nfl.teams.edit')
                        <a href="{{ route('dashboard.nfl.teams.edit', $team->id) }}" aria-label="@lang('rapture::actions.edit')">
                            <em class="far fa-pencil" aria-hidden="true" title="@lang('rapture::actions.edit_model', ['model' => $team->name])"></em>
                        </a>
                        @endcan
                        @can('nfl.teams.destroy')
                        <delete-button action="{{ route('dashboard.nfl.teams.destroy', $team->id) }}" aria-label="@lang('rapture::actions.delete')" title="@lang('rapture::actions.delete_model', ['model' => $team->name])"></delete-button>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </table>

        {{ $teams->links() }}
    </div>
@endsection

@push('styles')
    <style>
        .team-logo {
            width: 4rem;
        }
    </style>
@endpush
