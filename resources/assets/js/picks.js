/* global axios */
import Tabs from './tabs';

const matches = document.querySelectorAll('.matches button');

Array.from(matches).forEach((button) => {
    button.addEventListener('click', () => {
        const parent = button.closest('li');
        const activeButton = parent.querySelector('button.active');

        if (activeButton) {
            activeButton.classList.remove('active');
            activeButton.classList.remove('saved');
        }

        button.classList.add('active');

        axios.post('/dashboard/nfl/picks', {
            game: parent.getAttribute('data-match'),
            team: button.getAttribute('data-id'),
        }).then(() => {
            button.classList.add('saved');
        });
    });
});

const container = document.querySelector('.content-main');
const tabs = new Tabs(container, {
    heading: 'ul.tab-headings',
    panel: 'div.tab-body',
});
