const matches = document.querySelectorAll('.matches button');

Array.from(matches).forEach((button) => {
    button.addEventListener('click', () => {
        const parent = button.closest('li');
        const activeButton = parent.querySelector('button.active');

        if (activeButton) {
            activeButton.classList.remove('active');
        }

        button.classList.add('active');
        parent.querySelector('input').value = button.getAttribute('data-id');
    });
});
