<?php

return [
    'menu' => 'Games',
    'new_item' => 'New Week',
    'new_placeholder' => 'Week',
    'permission' => [
        'index' => 'Access to games',
        'create' => 'Create new games',
        'edit' => 'Modify existing games',
        'destroy' => 'Delete games',
    ],
    'index' => [
        'title' => 'Games',
    ],
    'edit' => [
        'title' => 'Edit Game',
        'success' => 'Game was successfully updated!',
    ],
    'delete' => [
        'success' => 'Game was successfully deleted!',
    ],
];
