<?php

return [
    'menu' => 'Results',
    'permission' => [
        'index' => 'View leaderboards',
    ],
];
