<?php

return [
    'menu' => 'Teams',
    'new_item' => 'New Team',
    'new_placeholder' => 'Team name',
    'permission' => [
        'index' => 'Access to teams',
        'create' => 'Create new teams',
        'edit' => 'Modify existing teams',
        'destroy' => 'Delete teams',
    ],
    'field' => [
        'logo' => 'Logo',
        'location' => 'Location',
    ],
    'index' => [
        'title' => 'Teams',
    ],
    'edit' => [
        'title' => 'Edit Team',
        'success' => 'Team was successfully updated!',
    ],
    'delete' => [
        'success' => 'Team was successfully deleted!',
    ],
];
