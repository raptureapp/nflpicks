<?php

return [
    'menu' => 'Picks',
    'new_item' => 'New Pick',
    'new_placeholder' => 'Pick name',
    'permission' => [
        'index' => 'Access to picks',
        'create' => 'Create new picks',
        'edit' => 'Modify existing picks',
        'destroy' => 'Delete picks',
    ],
    'index' => [
        'title' => 'Picks',
    ],
    'edit' => [
        'title' => 'Edit Pick',
        'success' => 'Pick was successfully updated!',
    ],
    'delete' => [
        'success' => 'Pick was successfully deleted!',
    ],
];
