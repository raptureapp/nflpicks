<?php

namespace Rapture\NFLFantasy\Commands;

use Illuminate\Console\Command;
use Rapture\Core\Installer;

class InstallCommand extends Command
{
    use Installer;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rapture:nfl:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'NFL fantasy league';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $menu = $this->addMenu([
            'label' => 'nfl::package.menu',
            'route' => 'dashboard.nfl.picks.index',
            'icon' => 'football-helmet',
            'namespaces' => ['dashboard/nfl'],
        ]);

        $this->addMenuItem([
            'menu_id' => $menu->id,
            'label' => 'nfl::picks.menu',
            'route' => 'dashboard.nfl.picks.index',
            'namespaces' => ['dashboard/nfl/picks'],
            'position' => 0,
        ]);

        $this->addMenuItem([
            'menu_id' => $menu->id,
            'label' => 'nfl::results.menu',
            'route' => 'dashboard.nfl.results.index',
            'namespaces' => ['dashboard/nfl/results'],
            'position' => 0,
        ]);

        $this->addMenuItem([
            'menu_id' => $menu->id,
            'label' => 'nfl::games.menu',
            'route' => 'dashboard.nfl.games.index',
            'namespaces' => ['dashboard/nfl/games'],
            'position' => 1,
        ]);

        $this->addMenuItem([
            'menu_id' => $menu->id,
            'label' => 'nfl::teams.menu',
            'route' => 'dashboard.nfl.teams.index',
            'namespaces' => ['dashboard/nfl/teams'],
            'position' => 2,
        ]);

        $this->resourcePermissions('nfl.picks');
        $this->resourcePermissions('nfl.games');
        $this->resourcePermissions('nfl.teams');

        $this->registerPermission('nfl.results', 'index', 'rapture::permission.index');

        $this->info('NFL Fantasy installed!');
    }
}
