<?php

namespace Rapture\NFLFantasy;

use Illuminate\Support\ServiceProvider;
use Rapture\NFLFantasy\Commands\InstallCommand;

class NFLFantasyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'nfl');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'nfl');

        $this->publishes([
            __DIR__ . '/../public' => public_path('nfl'),
        ], 'rapture');

        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
            ]);
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
