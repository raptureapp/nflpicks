<?php

namespace Rapture\NFLFantasy\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rapture\NFLFantasy\Models\Game;
use Rapture\NFLFantasy\Models\Team;
use Rapture\NFLFantasy\Models\Week;

class GamesController extends Controller
{
    public function index()
    {
        $weeks = Week::paginate(16);

        return view('nfl::games.index', compact('weeks'));
    }

    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'label' => 'required|max:255',
        ]);

        $week = Week::create($validatedData);

        return redirect()->route('dashboard.nfl.games.edit', $week->id);
    }

    public function edit(Week $game)
    {
        $teams = Team::orderBy('id')->get();

        return view('nfl::games.edit', [
            'week' => $game,
            'teams' => $teams,
        ]);
    }

    public function update(Request $request, Week $game)
    {
        $validatedData = $this->validate($request, [
            'label' => 'required|max:255',
        ]);

        if ($request->filled('cut_off')) {
            $game->cut_off = $request->input('cut_off');
        }

        $game->fill($validatedData);
        $game->save();

        $games = [];

        foreach ($request->input('match') as $match) {
            if (is_null($match['id'])) {
                $matchRow = Game::create([
                    'week_id' => $game->id,
                    'team1_id' => $match['team1'],
                    'team2_id' => array_key_exists('team2', $match) ? $match['team2'] : null,
                ]);

                $games[] = $matchRow->id;
            } else {
                $matchRow = Game::where('id', $match['id'])->update([
                    'team1_id' => $match['team1'],
                    'team2_id' => array_key_exists('team2', $match) ? $match['team2'] : null,
                ]);

                $games[] = $match['id'];
            }
        }

        Game::where('week_id', $game->id)->whereNotIn('id', $games)->delete();

        return redirect()
            ->route('dashboard.nfl.games.index')
            ->with('status', __('nfl::games.edit.success'));
    }

    public function destroy(Week $game)
    {
        $game->delete();

        return redirect()
            ->route('dashboard.nfl.games.index')
            ->with('status', __('nfl::games.delete.success'));
    }
}
