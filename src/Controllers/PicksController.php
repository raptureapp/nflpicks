<?php

namespace Rapture\NFLFantasy\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Rapture\NFLFantasy\Models\Pick;
use Rapture\NFLFantasy\Models\Week;

class PicksController extends Controller
{
    public function index()
    {
        $weeks = Week::orderBy('cut_off', 'asc')
            ->with(['games', 'games.team1', 'games.team2', 'games.pick'])
            ->get();

        $currentWeek = $weeks->first(function ($week) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $week->cut_off)->addHours(19) > now();
        });

        return view('nfl::picks.index', compact('weeks', 'currentWeek'));
    }

    public function store(Request $request)
    {
        $pick = Pick::updateOrCreate([
            'user_id' => auth()->user()->id,
            'game_id' => $request->input('game'),
        ], [
            'team_id' => $request->input('team'),
        ]);

        return response()->json($pick);
    }
}
