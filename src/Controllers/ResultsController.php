<?php

namespace Rapture\NFLFantasy\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Rapture\NFLFantasy\Models\Pick;
use Rapture\NFLFantasy\Models\Week;

class ResultsController extends Controller
{
    public function index()
    {
        $weeks = Week::orderBy('cut_off', 'asc')->get();
        $picks = Pick::with(['game', 'user'])->has('game')->get();

        $winners = $picks->groupBy(function ($item) {
            return $item->game->week_id;
        })->map(function ($week) {
            return $week->filter(function ($pick) {
                return !is_null($pick->game->winner_id) && $pick->team_id === $pick->game->winner_id;
            })->groupBy(function ($pick) {
                return $pick->user_id;
            })->map(function ($pick) {
                return [
                    'user' => $pick->first()->user,
                    'correct' => $pick->unique('game_id')->count(),
                ];
            })->sortByDesc('correct')->groupBy(function ($pick) {
                return $pick['correct'];
            });
        });

        $currentWeek = $weeks->reverse()->first(function ($week) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $week->cut_off)->addHours(19) < now();
        });

        return view('nfl::results.index', compact('weeks', 'currentWeek', 'winners'));
    }
}
