<?php

namespace Rapture\NFLFantasy\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rapture\NFLFantasy\Models\Team;

class TeamsController extends Controller
{
    public function index()
    {
        $teams = Team::paginate(16);

        return view('nfl::teams.index', compact('teams'));
    }

    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $team = Team::create($validatedData);

        return redirect()->route('dashboard.nfl.teams.edit', $team->id);
    }

    public function edit(Team $team)
    {
        return view('nfl::teams.edit', [
            'team' => $team,
        ]);
    }

    public function update(Request $request, Team $team)
    {
        $validatedData = $this->validate($request, [
            'name' => 'required|max:255',
            'location' => 'required|max:255',
            'logo' => 'required',
        ]);

        $team->fill($validatedData);
        $team->save();

        return redirect()
            ->route('dashboard.nfl.teams.index')
            ->with('status', __('nfl::teams.edit.success'));
    }

    public function destroy(Team $team)
    {
        $team->delete();

        return redirect()
            ->route('dashboard.nfl.teams.index')
            ->with('status', __('nfl::teams.delete.success'));
    }
}
