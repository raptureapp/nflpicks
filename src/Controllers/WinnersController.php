<?php

namespace Rapture\NFLFantasy\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Rapture\NFLFantasy\Models\Game;
use Rapture\NFLFantasy\Models\Pick;
use Rapture\NFLFantasy\Models\Team;
use Rapture\NFLFantasy\Models\Week;

class WinnersController extends Controller
{
    public function edit($week_id)
    {
        $teams = Team::orderBy('id')->get();
        $week = Week::find($week_id);

        return view('nfl::winners.edit', [
            'week' => $week,
            'teams' => $teams,
        ]);
    }

    public function update(Request $request, $week_id)
    {
        foreach ($request->input('game') as $id => $winner) {
            $match = Game::where('week_id', $week_id)->where('id', $id)->update(['winner_id' => $winner]);
        }

        return redirect()
            ->route('dashboard.nfl.games.index')
            ->with('status', 'Winners were successfully saved!');
    }
}
