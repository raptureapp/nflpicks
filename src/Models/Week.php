<?php

namespace Rapture\NFLFantasy\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    protected $guarded = [];

    public function games()
    {
        return $this->hasMany('Rapture\NFLFantasy\Models\Game');
    }

    public function allowed()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->cut_off)->addHours(19) > now();
    }

    public function locked()
    {
        return !$this->allowed();
    }

    public function closes($format = 'F j, Y')
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->cut_off)->format($format);
    }
}
