<?php

namespace Rapture\NFLFantasy\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $guarded = [];
}
