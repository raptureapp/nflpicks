<?php

namespace Rapture\NFLFantasy\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $guarded = [];

    public function team1()
    {
        return $this->belongsTo('Rapture\NFLFantasy\Models\Team', 'team1_id');
    }

    public function team2()
    {
        return $this->belongsTo('Rapture\NFLFantasy\Models\Team', 'team2_id');
    }

    public function week()
    {
        return $this->belongsTo('Rapture\NFLFantasy\Models\Week');
    }

    public function picks()
    {
        return $this->hasMany('Rapture\NFLFantasy\Models\Pick');
    }

    public function pick()
    {
        return $this->hasOne('Rapture\NFLFantasy\Models\Pick')->where('user_id', auth()->user()->id);
    }
}
