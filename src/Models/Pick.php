<?php

namespace Rapture\NFLFantasy\Models;

use Illuminate\Database\Eloquent\Model;

class Pick extends Model
{
    protected $guarded = [];

    public function game()
    {
        return $this->belongsTo('Rapture\NFLFantasy\Models\Game');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
