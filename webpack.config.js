const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');

const config = {
    mode: 'production',
    entry: {
        app: './resources/assets/js/app',
        picks: './resources/assets/js/picks',
        winners: './resources/assets/js/winners',
    },
    output: {
        path: path.resolve(__dirname, 'public/js'),
        filename: '[name].js',
        chunkFilename: '[id].chunk.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
            {
                test: /\.vue$/,
                exclude: /node_modules/,
                use: 'vue-loader',
            },
        ],
    },
    plugins: [
        new VueLoaderPlugin(),
    ],
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
        },
    },
};

module.exports = config;
